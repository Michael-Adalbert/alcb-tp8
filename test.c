#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#define SIZE_BUFF 20
#define SIZE_PATH 64

char buff[SIZE_BUFF];

char *message = "Hello World !";

void write_message(int f)
{
    if (write(f, message, 14) > 0)
    {
        fprintf(stdout, "write ok\n");
    }
    else
    {
        fprintf(stdout, "write not ok\n");
    }
}

void read_message(int f)
{
    int n_byte;
    while ((n_byte = read(f, buff, SIZE_BUFF)) > 0)
    {
        fprintf(stdout, "Lu (%d) : %s\n", n_byte, buff);
    }
}

int main(int argc, char **argv)
{
    int f;
    char select; 
    select = 0;
    printf("press : o to open \n");
    printf("press : c to close\n");
    printf("press : r to read \n");
    printf("press : w to write\n");
    printf("press : Ctrl-D (EOF) to exit\n");

    
    while((select = getchar()) != EOF){
        
        
        if (select == 'o')
        {
            getchar();
            char path_to_dev[SIZE_PATH];
            scanf("%[^\n]%*c", path_to_dev);
            if((f = open(path_to_dev, O_RDWR)) == -1){
                printf("cannot open the file\n");
            } else {
                printf("the file is open \n");
            }
        } 
        else if (select == 'c') 
        {
           if(f == -1){
                printf("no file is open\n");
                continue;
            }
            printf("close file \n");
            close(f);
        } 
        else if (select == 'w') 
        {
            if(f == -1){
                printf("no file is open\n");
                continue;
            }
            printf("write message \n");
            write_message(f);

        } 
        else if (select == 'r')
        {
            if(f == -1){
                printf("no file is open\n");
                continue;
            }
            printf("read message \n");
            read_message(f);
        } 
        else if (select != '\n')
        {
            printf("available input\n");
            printf("press : o to open \n");
            printf("press : c to close\n");
            printf("press : r to read \n");
            printf("press : w to write\n");
            printf("press : Ctrl-D (EOF) to exit\n");
            continue;
        }
    }
    return 0;
}